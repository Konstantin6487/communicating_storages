import { createStore, combineReducers } from 'redux';

const firstReducer = (state = 0, action) => {
  switch(action.type) {
    case 'INC':
      return state + action.payload;
    case 'DEC':
      return state - action.payload;
    default:
      return state;
  }
};

const secondReducer = (state = 1, action) => {
  switch(action.type) {
    case 'MUL':
      return state * action.payload;
    case 'DIV':
      return state / action.payload;
    default:
      return state;
  }
};

export const firstStore = createStore(
  combineReducers({ first: firstReducer }),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__({ name: 'store_1' }),
);

export const secondStore = createStore(
  combineReducers({ second: secondReducer }),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__({ name: 'store_2'}),
);
