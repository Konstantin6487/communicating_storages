import React, { Component } from 'react';
import { connect } from 'react-redux';

class First extends Component {

  componentDidMount() {
    const { dispatch, fluxDispatcher } = this.props;
    fluxDispatcher.register(dispatch);
  }

  handleClick = () => {
    const { fluxDispatcher } = this.props;
    fluxDispatcher.dispatch({ type: 'MUL', payload: 5 });
  }

  render() {
    const { state } = this.props;
    return (
      <div>
        <p><b>Component N1 with first store</b></p>
        <p>Текущий счетчик - {state}</p>
        <button onClick={this.handleClick}>Послать сообщение компоненту N2</button>
      </div>
    );
  }
}

export default connect(state => ({ state: state.first }))(First);