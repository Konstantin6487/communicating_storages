import React from 'react';
import { Provider } from 'react-redux';
import { Dispatcher } from 'flux';
import First from './First';
import Second from './Second';
import { secondStore, firstStore } from './stores';

const dispatcher = new Dispatcher();

export default () => (
  <>
    <Provider store={firstStore}>
      <First fluxDispatcher={dispatcher} />
    </Provider>
    <hr />
    <Provider store={secondStore}>
      <Second fluxDispatcher={dispatcher} />
    </Provider>
  </>
);