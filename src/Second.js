import React, { Component } from 'react';
import { connect } from 'react-redux';

class Second extends Component {

  componentDidMount() {
    const { dispatch, fluxDispatcher } = this.props;
    fluxDispatcher.register(dispatch);
  }

  handleClick = () => {
    const { fluxDispatcher } = this.props;
    fluxDispatcher.dispatch({ type: 'INC', payload: 5 });
  }

  render() {
    const { state } = this.props;
    return (
      <div>
        <p><b>Component N2 with second store</b></p>
        <p>Текущий счетчик - {state}</p>
        <button onClick={this.handleClick}>Послать сообщение компоненту N1</button>
      </div>
    );
  }
}

export default connect(state => ({ state: state.second }))(Second);